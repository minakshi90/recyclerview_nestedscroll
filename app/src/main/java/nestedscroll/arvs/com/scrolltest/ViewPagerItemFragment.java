package nestedscroll.arvs.com.scrolltest;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;


/**
 * A simple {@link Fragment} subclass.
 */
public class ViewPagerItemFragment extends Fragment {

    // widgets
    private ImageView mImage;


    // vars
    private Product mProduct;

    public static ViewPagerItemFragment getInstance(Product product){
        ViewPagerItemFragment fragment = new ViewPagerItemFragment();

        if(product != null){
            Bundle bundle = new Bundle();
            bundle.putParcelable("product", product);
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            mProduct = getArguments().getParcelable("product");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_view_pager_item, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mImage = view.findViewById(R.id.imageView3);
        init();
    }

    private void init(){
        if(mProduct != null){
            RequestOptions requestOptions = new RequestOptions()
                    .placeholder(R.drawable.ic_launcher_background);

            Glide.with(getActivity())
                    .setDefaultRequestOptions(requestOptions)
                    .load(mProduct.getImage())
                    .into(mImage);


        }
    }
}
