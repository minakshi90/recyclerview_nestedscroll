package nestedscroll.arvs.com.scrolltest;

public class item {
    String name;
    int product_image;
    String description;

    public item() {
    }

    public item(String name, int product_image, String description) {
        this.name = name;
        this.product_image = product_image;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProduct_image() {
        return product_image;
    }

    public void setProduct_image(int product_image) {
        this.product_image = product_image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
