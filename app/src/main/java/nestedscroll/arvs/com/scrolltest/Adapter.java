package nestedscroll.arvs.com.scrolltest;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.myViewHolder> {
    Context mContext;
    List<item> mData;

    public Adapter(Context mContext, List<item> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public myViewHolder onCreateViewHolder( ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(mContext);
        View v=inflater.inflate(R.layout.recycler_view_item,viewGroup,false);
        return new myViewHolder(v);
    }

    @Override
    public void onBindViewHolder(myViewHolder myViewHolder, int i) {
        myViewHolder.name.setText(mData.get(i).getName());
        myViewHolder.desc.setText(mData.get(i).getDescription());
        myViewHolder.image.setImageResource(mData.get(i).getProduct_image());


    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class myViewHolder extends RecyclerView.ViewHolder
    {
        TextView name,desc;
        ImageView image;


        public myViewHolder(View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.product_name);
            desc=itemView.findViewById(R.id.product_description);
            image=itemView.findViewById(R.id.product_image);

        }
    }
}
