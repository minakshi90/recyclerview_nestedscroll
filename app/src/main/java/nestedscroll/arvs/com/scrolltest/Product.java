package nestedscroll.arvs.com.scrolltest;

import android.os.Parcel;
import android.os.Parcelable;

public class Product implements Parcelable {
    private int image;

    public Product() {
    }

    public Product(int image) {
        this.image = image;
    }

    protected Product(Parcel in) {
        image = in.readInt();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(image);

    }
}
