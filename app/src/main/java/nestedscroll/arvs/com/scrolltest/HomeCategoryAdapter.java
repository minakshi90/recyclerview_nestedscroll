package nestedscroll.arvs.com.scrolltest;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
//import com.arvs.basketorganic.R;

public class HomeCategoryAdapter extends RecyclerView.Adapter<HomeCategoryAdapter.MyViewHolder> {
   // private ArrayList<HomeCategoryModel> arrayList;
    private int arrayList[]={R.drawable.fruits_image,R.drawable.vagitable_imgae,R.drawable.pulses_image,
           R.drawable.image1,R.drawable.oil_ghee,R.drawable.image3,R.drawable.image3};
    private String arrayList_category[]={"Fruits","Vegitable","Pulses","Dry Fruits","Oil & Ghee","Pulses","Suger Products"};

    private Context context;
    private LayoutInflater layoutInflater;

    public HomeCategoryAdapter(Context context){
        this.arrayList = arrayList;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.rv_home_category, null, false);
        MyViewHolder myViewHolder=new MyViewHolder(view);
        return myViewHolder ;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        //holder.txt_category.setText(arrayList.get(position).getTxt_cat());
        holder.txt_category.setText(arrayList_category[position]);
        holder.image_category.setImageResource(arrayList[position]);
    }

    @Override
    public int getItemCount() {
        return arrayList.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_category;
        ImageView image_category;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_category=itemView.findViewById(R.id.txt_category);
            image_category=itemView.findViewById(R.id.image_category);
         }
    }
}
