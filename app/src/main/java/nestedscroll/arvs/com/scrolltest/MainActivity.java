package nestedscroll.arvs.com.scrolltest;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    NestedScrollView nsv;
    private ViewPager mMyViewPager;
    private TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView=findViewById(R.id.recyclerView2);
        RecyclerView recyclerView2=findViewById(R.id.recyclerView);
        mTabLayout = findViewById(R.id.tab_layout);
        mMyViewPager = findViewById(R.id.view_pager);

        final List<item> mlist= new ArrayList<>();
        mlist.add(new item("Apple",R.drawable.apple,"description description description description \ndescription description description description "));
        mlist.add(new item("Apple",R.drawable.apple,"description description description description \ndescription description description description "));
        mlist.add(new item("Apple",R.drawable.apple,"description description description description  \ndescription description description description "));
        mlist.add(new item("Apple",R.drawable.apple,"description description description\ndescription description description"));
        mlist.add(new item("Apple",R.drawable.apple,"description description description\ndescription description description"));
        mlist.add(new item("Apple",R.drawable.apple,"description description description\ndescription description description"));
        mlist.add(new item("Apple",R.drawable.apple,"description description description\ndescription description description"));
        mlist.add(new item("Apple",R.drawable.apple,"description description description\ndescription description description"));
        mlist.add(new item("Apple",R.drawable.apple,"description description description\ndescription description description"));
        mlist.add(new item("Apple",R.drawable.apple,"description description description\ndescription description description"));
        mlist.add(new item("Apple",R.drawable.apple,"description description description\ndescription description description"));
        mlist.add(new item("Apple",R.drawable.apple,"description description description\ndescription description description"));
        mlist.add(new item("Apple",R.drawable.apple,"description description description\ndescription description description"));

        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
        Adapter adapter=new Adapter(this,mlist);
        recyclerView.setFocusable(false);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        HomeCategoryAdapter driveAdapter = new HomeCategoryAdapter(this);
        recyclerView2.setLayoutManager(new LinearLayoutManager(this, LinearLayout.HORIZONTAL, false));
        recyclerView2.setAdapter(driveAdapter);
        init();

    }

    private void init() {
        ArrayList<Fragment> fragments = new ArrayList<>();
        Product[] products = Products.getProducts();
        for(Product product: products){
            ViewPagerItemFragment fragment = ViewPagerItemFragment.getInstance(product);
            fragments.add(fragment);
        }
        MyPagerAdapter pagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), fragments);
        mMyViewPager.setAdapter(pagerAdapter);
        mTabLayout.setupWithViewPager(mMyViewPager, true);
    }
}
