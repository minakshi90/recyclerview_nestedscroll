package nestedscroll.arvs.com.scrolltest;

import android.widget.TextView;

public class HomeCategoryModel {
    TextView txt_cat;
    int image_cat;

    public HomeCategoryModel(TextView txt_cat, int image_cat) {
        this.txt_cat = txt_cat;
        this.image_cat = image_cat;
    }

    public TextView getTxt_cat() {
        return txt_cat;
    }

    public void setTxt_cat(TextView txt_cat) {
        this.txt_cat = txt_cat;
    }

    public int getImage_cat() {
        return image_cat;
    }

    public void setImage_cat(int image_cat) {
        this.image_cat = image_cat;
    }
}
